"""
Data Configuration
"""

# Libraries
from typing import Any
import json


def lambda_handler(event: dict, context: Any) -> dict:
    """Sample pure Lambda function"""
    return {
        "statusCode": 200,
        "body": json.dumps({
            "message": "hello world",
            "configuration": {
                "other": "more data",
            },
            "mount": "real time process",
            "event": event,
        }),
    }
