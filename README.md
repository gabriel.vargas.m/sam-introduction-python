# Introducción a SAM - Python

## Integrantes
- Juan Pineda (juan.pineda@smartquick.com.co)
- Gabriel Vargas (gabriel.vargas@smartquick.com.co)

## Descripción
Este código se base en ejemplos del [Repositorio Oficial de AWS](https://github.com/amazon-archives/serverless-app-examples/tree/master/python "Official Repository") que hacen referencia a los ejemplos enfocados y construidos por amazon labs para su respectiva guía de desarrollo.

Este [Documento](https://drive.google.com/file/d/1nAuvWoMlWNl-FjQQCU8Ip6mUSL0FI4LK/view?usp=sharing "Documento base") es el documento técnico donde se encuentran instucciones para instalación y ejecución del código.

## Requisitos
### Linux
- Python version 3.6 or higher
- Docker
- Docker Compose
- pip

### Windows
- Python version 3.6 or higher
- Docker
- Docker Compose
- awscli
- samcli
- pip

# Contenido
Este repositorio tiene código fuente relacionado a una aplicación serverless que esta construida para la herramienta conocida como AWS SAM

- hello_world - Código para la aplicación de la función lambda
- events - Eventos que puedes invocar para testeos de integración
- tests - Código relacionado a los testeos unitarios
- template.yaml - Un archivo que espefica la configuración relacionada

La aplicación utiliza varias herramientas usadas en AWS SAM, incluyendo "lambda functions" y "api gateway". Estos recursos están definidos en `template.yaml` archivo del proyecto. Se puede actualizar el archivo para incluir más herramientas de AWS SAM mediante todas las configuraciones que puede incluir AWS.

Se puede usar la extensión "AWS Toolkit" que integra funciones listas para desplegar y testear. Esto podría simplifcar estas tareas, sin embargo no se han explorado y queda de cuenta de cada uno instalar y usar la de su preferencia.

* [PyCharm](https://docs.aws.amazon.com/toolkit-for-jetbrains/latest/userguide/welcome.html)
* [IntelliJ](https://docs.aws.amazon.com/toolkit-for-jetbrains/latest/userguide/welcome.html)
* [VS Code](https://docs.aws.amazon.com/toolkit-for-vscode/latest/userguide/welcome.html)
* [Visual Studio](https://docs.aws.amazon.com/toolkit-for-visual-studio/latest/user-guide/welcome.html)

# Powered by
- **SmartQuick**
